import os
import logging
import asyncio
from datetime import datetime, timedelta

from aiogram import Bot, Dispatcher, types, F
from aiogram.filters import Command
from aiogram.types import Message
from pyrogram import Client
from pyrogram.enums import ChatMemberStatus


TOKEN = os.environ["TOKEN"]
API_ID = os.environ.get("API_ID", 2040)
API_HASH = os.environ.get("API_HASH", "b18441a1ff607e10a989891a5462e627")
CHAT_ID = int(os.environ.get("CHAT_ID", -1001672844707))

dp = Dispatcher()
logger = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.WARNING,
    format="%(asctime)s|%(levelname)s|%(name)s|%(message)s",
    datefmt='%Y-%m-%d|%H:%M:%S'
)


@dp.message(Command(commands=["start"]))
async def command_start_handler(message: Message) -> None:
    await message.answer("Go to @RuPremium!")


@dp.message(F.chat.id == CHAT_ID)
async def message_handler(message: types.Message, bot: Bot) -> None:
    if (message.from_user.is_bot or message.from_user.is_premium):
        return

    logger.warning("%s is going to be banned", message.from_user.username or message.from_user.id)
    await message.reply("Oh, you got kicked! Buy premium and join again.")
    await bot.ban_chat_member(message.chat.id, message.from_user.id, datetime.now() + timedelta(seconds=35))


async def worker(client: Client, chat_id: int) -> None:
    while True:
        async for member in client.get_chat_members(chat_id):
            if member.status != ChatMemberStatus.MEMBER or (member.user and member.user.is_bot):
                continue

            if (member.user and not member.user.is_premium):
                logger.warning("%s is going to be banned", member.user.username or member.user.id)
                await client.ban_chat_member(chat_id, member.user.id, datetime.now() + timedelta(seconds=35))

        await asyncio.sleep(600)


async def main() -> None:
    bot = Bot(TOKEN, parse_mode="HTML")
    client = Client("bot", api_id=API_ID, api_hash=API_HASH, bot_token=TOKEN, in_memory=True, parse_mode="HTML")

    try:
       logger.warning("Starting pyrogram client")
       await client.start()
       logger.warning("Starting aiogram polling")
       await asyncio.gather(
           dp.start_polling(bot, client=client),
           worker(client, CHAT_ID)
       )
    finally:
       await client.stop()


if __name__ == "__main__":
    asyncio.run(main())
